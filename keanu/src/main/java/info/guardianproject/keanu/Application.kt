package info.guardianproject.keanu

import org.matrix.android.sdk.api.Matrix
import org.matrix.android.sdk.api.MatrixConfiguration
import org.matrix.android.sdk.api.session.Session
import kotlin.properties.Delegates

open class Application: android.app.Application() {

    companion object {
        var matrix: Matrix? = null
        var session: Session? by Delegates.observable(null) { property, oldValue, newValue ->

            newValue?.open()
            newValue?.startSync(true)
        }
    }

    override fun onCreate() {
        super.onCreate()

        Matrix.initialize(this, MatrixConfiguration())

        matrix = Matrix.getInstance(this)

        session = matrix?.authenticationService()?.getLastAuthenticatedSession()
    }
}