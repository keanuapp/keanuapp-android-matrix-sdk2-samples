package info.guardianproject.keanu.ui.main

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import info.guardianproject.keanu.ui.chats.ChatsFragment

class MainAdapter(fm: FragmentManager) : FragmentPagerAdapter(fm, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {

    override fun getCount(): Int = 1

    override fun getItem(position: Int): Fragment {
        return ChatsFragment()
    }
}