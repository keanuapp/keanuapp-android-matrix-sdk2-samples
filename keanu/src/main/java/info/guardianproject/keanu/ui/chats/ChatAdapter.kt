package info.guardianproject.keanu.ui.chats

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.RecyclerView
import info.guardianproject.keanu.Application
import info.guardianproject.keanu.R
import org.matrix.android.sdk.api.session.room.RoomSummaryQueryParams
import org.matrix.android.sdk.api.session.room.model.RoomSummary

/**
 * [RecyclerView.Adapter] that can display a Chat summary.
 */
class ChatAdapter(fragment: Fragment) : RecyclerView.Adapter<ChatAdapter.ViewHolder>(),
    Observer<List<RoomSummary>> {

    var roomSummaries: List<RoomSummary> = ArrayList()

    init {
        val params = RoomSummaryQueryParams.Builder().build()
        val liveData = Application.session?.getRoomSummariesLive(params)
        liveData?.observe(fragment, this)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.fragment_chats, parent, false)

        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = roomSummaries[position]

        holder.idView.text = item.displayName
        holder.contentView.text = item.topic
    }

    override fun getItemCount(): Int {
        return roomSummaries.size
    }

    override fun onChanged(t: List<RoomSummary>?) {
        if (t != null) {
            roomSummaries = t

            notifyDataSetChanged()
        }
    }

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val idView: TextView = view.findViewById(R.id.item_number)
        val contentView: TextView = view.findViewById(R.id.content)

        override fun toString(): String {
            return super.toString() + " '" + contentView.text + "'"
        }
    }
}