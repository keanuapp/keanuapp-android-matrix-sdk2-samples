package info.guardianproject.keanu.ui.onboarding

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import info.guardianproject.keanu.databinding.ActivityWelcomeBinding

class WelcomeActivity : AppCompatActivity(), View.OnClickListener {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val binding = ActivityWelcomeBinding.inflate(layoutInflater)

        binding.nextButton.setOnClickListener(this)

        setContentView(binding.root)
    }

    override fun onClick(view: View?) {
        startActivity(Intent(this, AddAcountActivity::class.java))
        finish()
    }
}