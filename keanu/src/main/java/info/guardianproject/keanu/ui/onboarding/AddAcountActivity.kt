package info.guardianproject.keanu.ui.onboarding

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import info.guardianproject.keanu.R
import info.guardianproject.keanu.databinding.ActivityAddAcountBinding

class AddAcountActivity : AppCompatActivity(), View.OnClickListener {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val binding = ActivityAddAcountBinding.inflate(layoutInflater)

        binding.btnShowRegister.setOnClickListener(this)
        binding.btnShowLogin.setOnClickListener(this)

        setContentView(binding.root)
    }

    override fun onClick(view: View?) {
        when (view?.id) {
            R.id.btnShowRegister ->
                startActivity(Intent(this, AuthenticationActivity::class.java))

            R.id.btnShowLogin ->
                startActivity(Intent(this, AuthenticationActivity::class.java))
        }
    }
}