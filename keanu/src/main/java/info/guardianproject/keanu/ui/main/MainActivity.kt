package info.guardianproject.keanu.ui.main

import android.content.Intent
import android.os.Bundle
import android.os.StrictMode
import android.os.StrictMode.VmPolicy
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.google.android.material.tabs.TabLayout
import info.guardianproject.keanu.Application
import info.guardianproject.keanu.BuildConfig
import info.guardianproject.keanu.R
import info.guardianproject.keanu.databinding.ActivityMainBinding
import info.guardianproject.keanu.ui.onboarding.WelcomeActivity

open class MainActivity : AppCompatActivity(), View.OnClickListener,
    TabLayout.OnTabSelectedListener {

    lateinit var binding: ActivityMainBinding


    override fun onCreate(savedInstanceState: Bundle?) {

        if (BuildConfig.DEBUG) {
            StrictMode.setVmPolicy(VmPolicy.Builder().detectAll().penaltyLog().build())

            StrictMode.setThreadPolicy(
                StrictMode.ThreadPolicy.Builder().penaltyLog().penaltyFlashScreen()
                    .penaltyDeath().detectCustomSlowCalls().detectNetwork().build()
            )
        }

        super.onCreate(savedInstanceState)

        if (Application.session == null) {
            startActivity(Intent(this, WelcomeActivity::class.java))
            finish()

            return
        }

        binding = ActivityMainBinding.inflate(layoutInflater)

        binding.bannerClose.setOnClickListener(this)

        setSupportActionBar(binding.toolbar)

        binding.viewpager.adapter = MainAdapter(supportFragmentManager)

        binding.viewpager.addOnPageChangeListener(TabLayout.TabLayoutOnPageChangeListener(binding.tabs))

        var tab = binding.tabs.newTab()
        tab.setIcon(R.drawable.ic_discuss)
        tab.tag = getString(R.string.chats)
        tab.setContentDescription(R.string.chats)
        binding.tabs.addTab(tab)

        tab = binding.tabs.newTab()
        tab.setIcon(R.drawable.ic_people_white_36dp)
        tab.tag = getString(R.string.contacts)
        tab.setContentDescription(R.string.contacts)
        binding.tabs.addTab(tab)

        tab = binding.tabs.newTab()
        tab.setIcon(R.drawable.ic_explore_white_24dp)
        tab.tag = getString(R.string.title_more)
        tab.setContentDescription(R.string.title_more)
        binding.tabs.addTab(tab)

        tab = binding.tabs.newTab()
        tab.setIcon(R.drawable.ic_face_white_24dp)
        tab.tag = getString(R.string.title_me)
        tab.setContentDescription(R.string.title_me)
        binding.tabs.addTab(tab)

        binding.tabs.addOnTabSelectedListener(this)

        binding.fab.setOnClickListener(this)

        setContentView(binding.root)
    }

    override fun onClick(view: View?) {
        when (view?.id) {
            R.id.bannerClose ->
                binding.bannerLayout.visibility = View.GONE

            R.id.fab -> when (binding.viewpager.currentItem) {
                0 -> {
                    TODO("start ContactsPickerActivity")
                }
                1 -> {
                    TODO("inviteContact")
                }
                2 -> {
                    TODO("startPhotoTaker")
                }

            }
        }
    }

    override fun onTabSelected(tab: TabLayout.Tab?) {
        tab?.position?.let {
            binding.viewpager.setCurrentItem(it)
            setToolbarTitle(it)
        }
    }

    override fun onTabUnselected(tab: TabLayout.Tab?) {
        // Nothing to do. This should not happen.
    }

    override fun onTabReselected(tab: TabLayout.Tab?) {
        tab?.position?.let {
            setToolbarTitle(it)
        }
    }

    private fun setToolbarTitle(tabPosition: Int) {
        val sb = StringBuffer()
        sb.append(getString(R.string.app_name))
        sb.append(" | ")
        when (tabPosition) {
            0 -> {
                // TODO
//                if (mConversationList.getArchiveFilter()) {
//                    sb.append(getString(R.string.action_archive))
//                } else {
                    sb.append(getString(R.string.chats))
//                }
            }
            1 -> {
//                if (mContactList.getCurrentType() and Imps.Contacts.TYPE_FLAG_HIDDEN !== 0) {
//                    sb.append(getString(R.string.action_archive))
//                } else {
                    sb.append(getString(R.string.friends))
//                }
            }
            2 -> sb.append(getString(R.string.title_more))
            3 -> {
                sb.append(getString(R.string.me_title))
//                mAccountFragment.setUserVisibleHint(true)
            }
        }
        binding.toolbar.setTitle(sb.toString())

        if (tabPosition == 1) {
            binding.fab.setImageResource(R.drawable.ic_person_add_white_36dp)
        } else {
            binding.fab.setImageResource(R.drawable.ic_add_white_24dp)
        }
    }
}