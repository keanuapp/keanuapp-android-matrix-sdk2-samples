package info.guardianproject.keanu.ui.onboarding

import android.net.Uri
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import info.guardianproject.keanu.Application
import info.guardianproject.keanu.R
import info.guardianproject.keanu.databinding.ActivityAuthenticationBinding
import org.matrix.android.sdk.api.MatrixCallback
import org.matrix.android.sdk.api.auth.data.HomeServerConnectionConfig
import org.matrix.android.sdk.api.auth.data.LoginFlowResult
import org.matrix.android.sdk.api.session.Session

class AuthenticationActivity : AppCompatActivity(), View.OnClickListener {

    lateinit var binding: ActivityAuthenticationBinding

    val deviceName: String by lazy {
        getString(R.string.app_name) + " " + android.os.Build.MODEL
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityAuthenticationBinding.inflate(layoutInflater)

        binding.btnSignIn.setOnClickListener(this)

        setContentView(binding.root)
    }

    override fun onClick(view: View?) {
        val authService = Application.matrix?.authenticationService()

        if (authService == null) {
            return
        }

        var server = binding.edtServer.text.toString().trim()
        if (server.isEmpty()) {
            server = getString(R.string.default_server)
        }

        var homeServer = Uri.parse(server)
        if (homeServer.scheme != "http" && homeServer.scheme != "https") {
            homeServer = homeServer.buildUpon().scheme("https").build()
        }

        val config = HomeServerConnectionConfig.Builder()
            .withHomeServerUri(homeServer)
            .build()

        authService.getLoginFlow(config, object: MatrixCallback<LoginFlowResult> {

            override fun onSuccess(data: LoginFlowResult) {
                authService.getLoginWizard().login(
                    binding.edtName.text.toString().trim(),
                    binding.edtPass.text.toString().trim(),
                    deviceName,
                    object: MatrixCallback<Session> {

                        override fun onSuccess(data: Session) {
                            Application.session = data
                        }

                        override fun onFailure(failure: Throwable) {
                            TODO()
                        }
                    }
                )
            }

            override fun onFailure(failure: Throwable) {
                TODO()
            }
        })
    }
}
