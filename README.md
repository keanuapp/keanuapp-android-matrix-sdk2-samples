# Keanu Android Matrix SDK2 sample code

Keanu is a privacy-enhancing client for the Matrix protocol.

This is a fresh rewrite based on matrix-android-sdk2.


## Art Assets
** Sea Collection and Surfing by Laymik from the Noun Project
** Document iconfs from: https://dribbble.com/shots/4886612-FREEBIE-File-format-Flat-icons

## Dev Links

http://github.com/matrix-org/matrix-android-sdk2/
https://github.com/matrix-org/matrix-android-sdk2-sample
https://github.com/vector-im/element-android
